function addTokens(input, tokens) {
    if (typeof input !== "string") {
      throw "Invalid input";
    } else if (input.length < 6) {
      throw "Input should have at least 6 characters";
    }
    if (Array.isArray(tokens)) {
      tokens.forEach(token => {
        if (
          token.hasOwnProperty("tokenName") &&
          typeof token.tokenName === "string"
        ) {
          input = input.replace("...", "${" + token.tokenName + "}");
        } else {
          throw "Invalid array format";
        }
      });
    } else {
      throw "Invalid array format";
    }
    return input;
  }
   
  const app = {
    addTokens: addTokens
  };
   
  module.exports = app;